// var mysql = require('mysql');

// var connection = mysql.createConnection({
//     "host": "127.0.0.1",
//     "port": "3000",
//     "user": "root",
//     "database": "forvizdb"
// });

// connection.connect(); //<<<<<<<<<<<<<

// connection.query('SELECT * FROM `properties` LIMIT 10', function(err, results) {
//     results.forEach(function(product){
//         console.log(product.name);
//     });
// });

// connection.end(); //<<<<<<<<<<<<<

class Database {
    constructor( ) {    
                this.host = '127.0.0.1';
                this.user = 'root';
                this.database = 'forvizdb';
                this.connectDatabase(this.host,this.user,this.database);
    }

    connectDatabase(host,user,database) {
        // get the client
        // const mysql = require('mysql2/promise');
        const mysql = require('mysql2');

        // create the connection
        this.connection = mysql.createConnection({
            host: this.host,
            user: this.user,
            database: this.database
        });
    }
}

module.exports.db = new Database();