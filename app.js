const express = require('express');
const app = express();
const {db} = require("./lib/db.js");
const bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: true });

app.set('view engine', 'ejs');

app.get('/search', getAllData);
app.post('/search', urlencodedParser, getAllData);

function checkParam(req){
    let sqlCommand = `SELECT * FROM properties Where 1`;
    if(req.body){
        if(req.body.uniqId){ //uniq_id Search by uniq Id (?uniq_id=00954d18277acc4bf9080370b1e0bcfe)
            sqlCommand = sqlCommand + ` and uniq_id = '${req.body.uniqId}'`;
        }
        if(req.body.propertyType){ //property_type Search by property_type (?property_type=Hotel)
            sqlCommand = sqlCommand + ` and property_type = '${req.body.propertyType}'`;
        }
        if(req.body.city){ //city Search by City (?city=Kanpur)
            sqlCommand = sqlCommand + ` and city LIKE '%${req.body.city}%'`;
        }
        if(req.body.amenities){ //amenities Search by amenities (?amenities=Parking)
            sqlCommand = sqlCommand + ` and amenities LIKE '%${req.body.amenities}%'`;
        }
        //SELECT * FROM properties WHERE 1 and CONVERT(SUBSTRING_INDEX(room_price,'-',-1),UNSIGNED INTEGER) >= 500 AND CONVERT(SUBSTRING_INDEX(room_price,'-',-1),UNSIGNED INTEGER) <=600 LIMIT 10
        if(req.body.roomPriceMoreThan){ //room_price Search by Room Price Range min to max (?room_price=1000-2000)
            sqlCommand = sqlCommand + ` and CONVERT(SUBSTRING_INDEX(room_price,'-',-1),UNSIGNED INTEGER) >= ${req.body.roomPriceMoreThan}`;
        }
        if(req.body.roomPriceLessThan){ //room_price Search by Room Price Range min to max (?room_price=1000-2000)
            sqlCommand = sqlCommand + ` and CONVERT(SUBSTRING_INDEX(room_price,'-',-1),UNSIGNED INTEGER) <= ${req.body.roomPriceLessThan}`;
        }
        // if(req.body.Location){ //Location Search by Location & radius
        //     sqlCommand = sqlCommand + ' and uniq_id=' + req.body.uniqId;
        // }
    }
    sqlCommand = sqlCommand + ' LIMIT 10'
    return  sqlCommand;
}

function getAllData(req, res) {
    
    let sqlCommand = checkParam(req);
    
    const result = db.connection.query(sqlCommand, function(err, results) {
        if(err){
            return console.error('error running query', err);
        }
        var tagline = results;
        res.render('index', {
            tagline: tagline
        });
    });
}

app.listen(3000);